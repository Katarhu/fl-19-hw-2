import Layout from "./layout/Layout";
import {Routes, Route} from 'react-router-dom';
import Notes from "./pages/Notes/Notes";
import Login from "./pages/Login/Login";
import Register from "./pages/Register/Register";
import Forbidden from "./pages/Forbidden/Forbidden";
import Home from "./pages/Home/Home";
import {useAppDispatch, useAppSelector} from "./hooks/redux";
import {useEffect} from "react";
import {authUser} from "./redux/reducers/authSlice";
import ChangePassword from "./pages/ChangePassword/ChangePassword";

function App() {

    const dispatch = useAppDispatch();

    useEffect(() => {
        dispatch(authUser());
    }, [useAppDispatch]);

    return (
        <Layout>
            <Routes>
                <Route path="/notes" element={<Notes/>}/>
                <Route path="/" element={<Home/>}/>
                <Route path="/login" element={<Login/>}/>
                <Route path="/register" element={<Register/>}/>
                <Route path="/forbidden" element={<Forbidden/>}/>
                <Route path="/edit" element={<ChangePassword/>}/>
            </Routes>
        </Layout>
    )
}

export default App
