import './Note.scss';
import {Note as NoteType} from "../../models/Note";
import {useEffect, useMemo, useRef, useState} from "react";
import axios from "../../utils/axios";
import {useAppDispatch} from "../../hooks/redux";
import {deleteNoteById} from "../../redux/reducers/notesSlice";


function Note({_id, text, createdAt, updatedAt, completed}: NoteType) {

    const appDispatch = useAppDispatch();
    const noteTextElement = useRef<null | HTMLElement>(null);

    const [noteState, setNoteState] = useState({
        noteText: text,
        createdAt: createdAt,
        updatedAt: updatedAt,
        completed: completed,
        isLoading: false,
        error: ''
    });

    const [changes, setChanges] = useState(false);

    useEffect(() => {
        setNoteState(state => ({...state, isLoading: true}));
        (async () => {
            try {
                const {data} = await axios.get(`/notes/${_id}`);
                setNoteState(state => ({...state, noteText: data.note.text}));
                setNoteState(state => ({...state, createdAt: data.note.createdAt}));
                setNoteState(state => ({...state, completed: data.note.completed}));
                setNoteState(state => ({...state, updatedAt: data.note.updatedAt}));
                setTimeout(() => {
                    setNoteState(state => ({...state, isLoading: false}));
                }, 450);
                setNoteState(state => ({...state, error: ''}));
            } catch (err: any) {
                setNoteState(state => ({...state, isLoading: false}));
                setNoteState(state => ({...state, error: err.message}));
            }

        })()

    }, [changes]);

    async function changeNoteText() {
        if (noteTextElement.current!.textContent === noteState.noteText) {
            alert('Text didn\'t change');
            return
        }

        await axios.put(`/notes/${_id}`, {
            text: noteTextElement.current!.textContent,
        });

        setChanges(changes => !changes);
    }

    async function changeIsComplete() {
        await axios.patch(`/notes/${_id}`);
        setChanges(changes => !changes);
    }

    function deleteNote(id: string) {
        appDispatch(deleteNoteById(id));
    }

    return (
        <div className={`note ${noteState.isLoading ? 'loading' : ''}`}>
            {noteState.isLoading ?
                <div className="spinner">
                    <div className="bounce1"></div>
                    <div className="bounce2"></div>
                    <div className="bounce3"></div>
                </div>
                :
                <>
                    <div className="note__date">
                        {noteState.updatedAt > noteState.createdAt ? `Updated at ${noteState.updatedAt}` : `Created at ${noteState.createdAt}`}
                    </div>

                    <div className={`note__completed note__completed_${noteState.completed ? 'true' : 'false'}`}>
                        {noteState.completed ? 'Completed' : ' Not Completed'}
                    </div>

                    <span className="note__input"
                          role="textbox"
                          contentEditable
                          ref={noteTextElement}>
                {noteState.noteText}
                </span>

                    <div className="note__controls">
                        <button className="note__button note__button_delete" onClick={() => deleteNote(_id)}>Delete
                            note
                        </button>
                        <button className="note__button note__button_commit" onClick={changeNoteText}>
                            Commit changes
                        </button>
                        <button className="note__button" style={{background: noteState.completed ? 'gray' : 'green'}}
                                onClick={changeIsComplete}>
                            {!noteState.completed ?
                                'Set as complete' :
                                'Set as not complete'}
                        </button>
                    </div>
                </>}
        </div>
    )
}

export default Note;