import {useRef, useState} from "react";
import {Link, useNavigate} from 'react-router-dom';
import './Navbar.scss';
import {useAppDispatch, useAppSelector} from '../hooks/redux';
import {checkIsAuth, logout, deleteUser} from "../redux/reducers/authSlice";
import {clearNotes} from "../redux/reducers/notesSlice";

function Navbar() {
    const isAuth = useAppSelector(checkIsAuth);
    const dispatch = useAppDispatch();
    const {user, isLoading} = useAppSelector(state => state.auth);
    const navigate = useNavigate()

    const menuOptions = useRef(null);

    const [isMenuToggled, setIsMenuToggled] = useState(false);

    function toggleMenu() {
        setIsMenuToggled((isToggled) => !isToggled)
    }

    function logoutHandler() {
        dispatch(logout());
        dispatch(clearNotes());
        setIsMenuToggled(false);
        localStorage.removeItem('token');
        navigate('/');
    }

    async function deleteHandler() {
        if (confirm('Are you sure')) {
            await dispatch(deleteUser());
            logoutHandler();
        }
    }

    return (
        <nav className="nav">
            <div className="nav__logo">
                <Link className="nav__link" to={isAuth ? 'notes' : '/'}>{isAuth ? 'Notes' : 'HW2'}</Link>
            </div>
            {!isAuth ?
                <div className="nav__buttons">
                    <Link className="nav__btn nav__login" to="/login">Login</Link>
                    <Link className="nav__btn nav__register" to="/register">Register</Link>
                </div> :
                <div className="nav__user">
                    <div className="nav__menu menu">
                        <div className="menu__user user" onClick={toggleMenu}>
                            <span>▼</span>
                            <div className="user__username">
                                {user?.username}
                            </div>
                            <div className="user__logo">
                                {user?.username[0]}
                            </div>
                        </div>
                        <div className="menu__options" ref={menuOptions}
                             style={{display: isMenuToggled ? 'flex' : 'none'}}>
                            <div className="menu__option">
                                <Link className="menu__link menu__link_change" to="/edit">Change Password</Link>
                            </div>
                            <div className="menu__option">
                                <button className="menu__logout" onClick={logoutHandler}>Logout</button>
                            </div>
                            <div className="menu__option">
                                <Link className="menu__link menu__link_delete" to="/" onClick={deleteHandler}>Delete
                                    account</Link>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </nav>
    )
}

export default Navbar;