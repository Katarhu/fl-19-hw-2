export interface Note {
    _id: string;
    text: string;
    completed: boolean;
    createdAt: string;
    updatedAt: string;
    userId: string;
}

export interface NoteState {
    notes: Note[];
    isLoading: boolean;
    error: string;
}

export interface NotesResponse {
    offset: number;
    limit: number;
    notes: Note[];
    count: number
}

export interface NoteResponse {
    note: Note;
}