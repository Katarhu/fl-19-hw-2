export interface UserType {
    username: string;
    id: string;
}

export interface AuthResponseType {
    user: UserType;
    token?: string | null;
    jwt_token?: string | null;
}

export interface UserState {
    user: null | UserType;
    token: null | string | undefined;
    isLoading: boolean;
    error: string;
}