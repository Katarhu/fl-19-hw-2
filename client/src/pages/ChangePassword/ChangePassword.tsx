import {useNavigate} from "react-router-dom";
import {useReducer} from 'react';
import {useAppSelector} from "../../hooks/redux";
import {checkIsAuth} from "../../redux/reducers/authSlice";
import {RefObject, useEffect, useRef} from "react";
import './ChangePassword.scss';
import '../Form.scss';
import changeReducer from './changePasswordReducer'
import {fillNewPassword, fillOldPassword, fillError} from './changePasswordReducer';
import axios from "../../utils/axios";

function ChangePassword() {
    const navigate = useNavigate();
    const passwordField = useRef(null);
    const passwordRepeatField = useRef(null);
    const isAuth = useAppSelector(checkIsAuth);


    useEffect(() => {
        if (!isAuth) {
            navigate('/notes')
        }
    }, [isAuth]);

    const initialState = {
        newPassword: '',
        oldPassword: '',
        error: ''
    }

    const [inputState, dispatch] = useReducer(changeReducer, initialState);

    function togglePasswordField(this: RefObject<HTMLInputElement>) {
        try {
            this.current!.type = this.current!.type === 'password' ? 'text' : 'password';
        } catch (err) {
            console.log(err);
        }
    }

    async function changePassword() {
        console.log('hello')
        try {
            if (!inputState.newPassword || !inputState.oldPassword) {
                return dispatch(fillError('Please fill all inputs'));
            }

            await axios.patch('/users/me', {
                newPassword: inputState.newPassword,
                oldPassword: inputState.oldPassword
            });

            return dispatch(fillError('Password was changed'));
        } catch (err) {
            return dispatch(fillError('Invalid password'));
        }
    }

    return (
        <div className="change">
            <div className="change__container">
                <form className="register__form form" onSubmit={(e) => {
                    e.preventDefault();
                }}>
                    <h2 className="form__title">Change password</h2>

                    <fieldset className="form__inputs">

                        <input className="form__input form__password"
                               id="pass-input"
                               type="password"
                               placeholder=" "
                               ref={passwordField}
                               onChange={(e) => {
                                   dispatch(fillOldPassword(e.currentTarget.value))
                               }}/>
                        <label className="form__label" htmlFor="pass-input">Old password</label>
                        <label className="form__show-pass"
                               onClick={() => togglePasswordField.call(passwordField)}>👁</label>

                        <input className="form__input form__password"
                               id="pass-rep-input"
                               type="password"
                               placeholder=" "
                               ref={passwordRepeatField}
                               onChange={(e) => {
                                   dispatch(fillNewPassword(e.currentTarget.value))
                               }}/>
                        <label className="form__label" htmlFor="pass-rep-input">New password</label>
                        <label className="form__show-pass"
                               onClick={() => togglePasswordField.call(passwordRepeatField)}>👁</label>

                        {inputState.error &&
                            <p className="form__error">{inputState.error}</p>
                        }

                        <input className="form__button"
                               type="submit"
                               value="Change password"
                               onClick={changePassword}/>

                    </fieldset>
                </form>
            </div>
        </div>
    )
}

export default ChangePassword;