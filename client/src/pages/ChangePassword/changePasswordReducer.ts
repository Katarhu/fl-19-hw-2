interface ChangeState {
    newPassword: string;
    oldPassword: string;
    error: string;
}

const initialState = {
    newPassword: '',
    oldPassword: '',
    error: ''
}

enum ChangeTypes {
    FILL_NEW_PASSWORD = 'FILL_NEW_PASSWORD',
    FILL_OLD_PASSWORD = 'FILL_OLD_PASSWORD',
    FILL_ERROR = 'FILL_ERROR'
}

interface FILL_NEW_PASSWORD_ACTION {
    type: ChangeTypes.FILL_NEW_PASSWORD;
    payload: string;
}

interface FILL_OLD_PASSWORD_ACTION {
    type: ChangeTypes.FILL_OLD_PASSWORD;
    payload: string;
}

interface FILL_ERROR_ACTION {
    type: ChangeTypes.FILL_ERROR;
    payload: string;
}

type ChangeAction = FILL_ERROR_ACTION
    | FILL_OLD_PASSWORD_ACTION
    | FILL_NEW_PASSWORD_ACTION

export default function changeReducer(state: ChangeState = initialState, action: ChangeAction) {
    switch (action.type) {
        case ChangeTypes.FILL_NEW_PASSWORD:
            return {...state, newPassword: action.payload, error: ''}
        case ChangeTypes.FILL_OLD_PASSWORD:
            return {...state, oldPassword: action.payload, error: ''}
        case ChangeTypes.FILL_ERROR:
            return {...state, error: action.payload}
        default:
            return state;
    }
}

export const fillNewPassword = (payload: string) => ({type: ChangeTypes.FILL_NEW_PASSWORD, payload})
export const fillOldPassword = (payload: string) => ({type: ChangeTypes.FILL_OLD_PASSWORD, payload})
export const fillError = (payload: string) => ({type: ChangeTypes.FILL_ERROR, payload})
