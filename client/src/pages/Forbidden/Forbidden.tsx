import './Forbidden.scss';
import {Link, useNavigate} from "react-router-dom";
import {useAppSelector} from "../../hooks/redux";
import {checkIsAuth} from "../../redux/reducers/authSlice";
import {useEffect} from "react";

function Forbidden() {
    const navigate = useNavigate();
    const isAuth = useAppSelector(checkIsAuth);

    useEffect(() => {
        if (isAuth) {
            navigate('/notes')
        }
    }, [isAuth])

    return (
        <div className="forbidden">
            <div className="forbidden__container">
                <h1 className="forbidden__title">403 Seems that you are not authorized</h1>
                <img className="forbidden__gif"
                     src="https://cdn.dribbble.com/users/285475/screenshots/2083086/dribbble_1.gif" alt="GIF"/>
                <Link className="forbidden__link" to="/">Go to home page</Link>
            </div>
        </div>

    )
}

export default Forbidden;