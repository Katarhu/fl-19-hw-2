import './Home.scss';
import {Link, useNavigate} from "react-router-dom";
import {useAppSelector} from "../../hooks/redux";
import {checkIsAuth} from "../../redux/reducers/authSlice";
import {useEffect} from "react";

function Home() {

    const navigate = useNavigate();
    const isAuth = useAppSelector(checkIsAuth);

    useEffect(() => {
        if (isAuth) {
            navigate('/notes')
        }
    }, [])

    return (
        <div className="home">
            <div className="home__container">
                <h1 className="home__title">Welcome to HW-2, to continue you have to <Link
                    to="login">login</Link> or <Link
                    to="register">register</Link></h1>
            </div>
        </div>
    )
}

export default Home;