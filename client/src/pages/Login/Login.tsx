import {RefObject, useRef, useReducer, useEffect} from 'react';
import './Login.scss';
import '../Form.scss';
import loginReducer, {fillUsername, setIsValid, fillPassword} from "./loginReducer";
import {useAppDispatch, useAppSelector} from "../../hooks/redux";
import {checkIsAuth, loginUser} from "../../redux/reducers/authSlice";
import {useNavigate} from "react-router-dom";


function Login() {
    const passwordField = useRef(null);
    const isAuth = useAppSelector(checkIsAuth);
    const appDispatch = useAppDispatch();
    const navigate = useNavigate();
    const {error} = useAppSelector(state => state.auth);

    useEffect(() => {
        if (isAuth) {
            if (isAuth) {
                navigate('/notes');
            }
        }
    }, [isAuth]);

    useEffect(() => {
        dispatch(setIsValid(error))
    }, [error])

    const [inputState, dispatch] = useReducer(loginReducer, {
        usernameInput: '',
        passwordInput: '',
        error: ''
    });

    function togglePasswordField(this: RefObject<HTMLInputElement>) {
        try {
            this.current!.type = this.current!.type === 'password' ? 'text' : 'password';
        } catch (err) {
            console.log(err);
        }
    }

    function checkInput() {
        if (!inputState.usernameInput || !inputState.passwordInput) {
            return dispatch(setIsValid('Wrong input'));
        }

        appDispatch(loginUser({username: inputState.usernameInput, password: inputState.passwordInput}));
        if (error) {
            dispatch(setIsValid(error));
            return;
        }
    }

    return (
        <div className="login">
            <form className="login__form form" onSubmit={(e) => {
                e.preventDefault();
                checkInput();
            }}>
                <h2 className="form__title">Log in</h2>

                <fieldset className="form__inputs">
                    <input className="form__input" id="email-input" type="text" placeholder=" "
                           onChange={(e) =>
                               dispatch(fillUsername(e.currentTarget.value))
                           }/>
                    <label className="form__label" htmlFor="email-input">Email</label>

                    <input className="form__input form__password" id="pass-input" type="password" placeholder=" "
                           ref={passwordField}
                           onChange={(e) =>
                               dispatch(fillPassword(e.currentTarget.value))
                           }/>
                    <label className="form__label" htmlFor="pass-input">Password</label>
                    <label className="form__show-pass"
                           onClick={() => togglePasswordField.call(passwordField)}>👁</label>

                    {inputState.error &&
                        <p className="form__error">{inputState.error}</p>
                    }

                    <input className="form__button" type="submit" value="Login"/>

                </fieldset>
            </form>
        </div>
    )
}

export default Login;