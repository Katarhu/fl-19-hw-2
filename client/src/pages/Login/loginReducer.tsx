import {LoginTypes, LoginState, LoginActionType} from './loginTypes';

function reducer(state: LoginState, action: LoginActionType): LoginState {
    switch (action.type) {
        case LoginTypes.FILL_USERNAME:
            return {...state, usernameInput: action.payload, error: ''};
        case LoginTypes.FILL_PASSWORD:
            return {...state, passwordInput: action.payload, error: ''};
        case LoginTypes.SET_IS_VALID:
            return {...state, error: action.payload}
        default:
            return state;
    }
}

export const fillUsername = (payload: string) => {
    return {type: LoginTypes.FILL_USERNAME, payload}
};

export const fillPassword = (payload: string) => {
    return {type: LoginTypes.FILL_PASSWORD, payload}
};

export const setIsValid = (payload: string) => {
    return {type: LoginTypes.SET_IS_VALID, payload}
};


export default reducer;