export interface LoginState {
    usernameInput: string;
    passwordInput: string;
    error: string;
}

export enum LoginTypes {
    FILL_USERNAME = 'FILL_USERNAME',
    FILL_PASSWORD = 'FILL_PASSWORD',
    SET_IS_VALID = 'SET_IS_VALID'
}

interface FILL_USERNAME_ACTION {
    type: LoginTypes.FILL_USERNAME,
    payload: string
}

interface FILL_PASSWORD_ACTION {
    type: LoginTypes.FILL_PASSWORD,
    payload: string
}

interface SET_IS_VALID_ACTION {
    type: LoginTypes.SET_IS_VALID;
    payload: string;
}

export type LoginActionType =
    FILL_USERNAME_ACTION
    | FILL_PASSWORD_ACTION
    | SET_IS_VALID_ACTION