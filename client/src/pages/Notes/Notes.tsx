import {useNavigate} from "react-router-dom";
import {useEffect, useMemo, useState} from "react";
import {useAppDispatch, useAppSelector} from "../../hooks/redux";
import {checkIsAuth} from "../../redux/reducers/authSlice";
import {createNote, getNotes} from "../../redux/reducers/notesSlice";
import Note from "../../components/Note/Note";
import './Notes.scss';
import axios from "../../utils/axios";

function Notes() {
    const notes = useAppSelector(state => state.note.notes)
    const navigate = useNavigate();
    const isAuth = useAppSelector(checkIsAuth);
    const appDispatch = useAppDispatch();

    const [isAddNote, setIsAddNote] = useState(false);
    const [notesAmount, setNotesAmount] = useState(5);
    const [newNoteText, setNewNoteText] = useState('');

    useEffect(() => {
        appDispatch(getNotes({limit: notesAmount, offset: 0}));

    }, [notesAmount]);

    useEffect(() => {
        if (!isAuth) {
            navigate('/forbidden')
        }
    }, [])

    function increaseNotesAmount() {
        setNotesAmount(prevAmount => prevAmount + 5);
    }

    function createNewNote() {
        appDispatch(createNote(newNoteText));
        setNewNoteText('');
    }

    return (
        <div className="notes">
            <h1 className="notes__title">My notes</h1>
            <button className="notes__button"
                    onClick={() => setIsAddNote(isAddNote => !isAddNote)}>
                {isAddNote ? 'x Hide' : 'Add new note'}
            </button>

            {isAddNote &&
                <form className="notes__form notes-form"
                      onSubmit={(e) => {
                          e.preventDefault();
                          createNewNote();
                      }}>
                    <h2 className="notes-form__title">Create new note</h2>
                    <textarea className="notes-form__input"
                              cols={6} rows={10}
                              placeholder="Note text"
                              onChange={(e) => setNewNoteText(e.currentTarget.value)}
                              value={newNoteText}
                    ></textarea>
                    <input className="notes-form__button"
                           type="submit"
                           value="Create"/>
                </form>}
            <div className="notes__container">
                {notes.map(note => (
                    <Note key={note._id} {...note}/>
                ))}

                <button className="notes__show" onClick={increaseNotesAmount}>Show more notes</button>
            </div>
        </div>
    )
}

export default Notes;