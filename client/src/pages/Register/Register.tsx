import './Register.scss';
import '../Form.scss';
import {RefObject, useRef, useReducer, useEffect} from 'react';
import registerReducer from "./registerReducer";
import {fillPassword, fillRepeatedPassword, fillUsername, setIsValid} from "./registerReducer";
import {useAppDispatch, useAppSelector} from "../../hooks/redux";
import {checkIsAuth, registerUser} from "../../redux/reducers/authSlice";
import {useNavigate} from 'react-router-dom';

function Register() {
    const passwordField = useRef(null);
    const passwordRepeatField = useRef(null);
    const appDispatch = useAppDispatch();
    const navigate = useNavigate();
    const isAuth = useAppSelector(checkIsAuth);
    const {error} = useAppSelector(state => state.auth);

    useEffect(() => {
        if (isAuth) {
            navigate('/notes');
        }
    }, [isAuth]);


    const [inputState, dispatch] = useReducer(registerReducer, {
        usernameInput: '',
        passwordInput: '',
        passwordRepeatInput: '',
        error: ''
    });

    useEffect(() => {
        dispatch(setIsValid(error));
    }, [error])


    function togglePasswordField(this: RefObject<HTMLInputElement>) {
        try {
            this.current!.type = this.current!.type === 'password' ? 'text' : 'password';
        } catch (err) {
            console.log(err);
        }
    }

    function isInputValid() {
        if (!inputState.usernameInput
            || !inputState.passwordInput
            || !inputState.passwordRepeatInput) {
            dispatch(setIsValid('Wrong input'));
            return;
        }

        if (inputState.passwordInput !== inputState.passwordRepeatInput) {
            dispatch(setIsValid('Passwords doesn\'t match'));
            return;
        }

        appDispatch(registerUser({username: inputState.usernameInput, password: inputState.passwordInput}));
        if (error) {
            dispatch(setIsValid(error));
            return;
        }
    }

    return (
        <div className="register">
            <form className="register__form form" onSubmit={(e) => {
                e.preventDefault();
                isInputValid()
            }}>
                <h2 className="form__title">Register</h2>

                <fieldset className="form__inputs">

                    <input className="form__input"
                           id="username-input"
                           type="text"
                           placeholder=" "
                           onChange={(e) => {
                               dispatch(fillUsername(e.currentTarget.value))
                           }}/>
                    <label className="form__label" htmlFor="username-input">Username</label>

                    <input className="form__input form__password"
                           id="pass-input"
                           type="password"
                           placeholder=" "
                           ref={passwordField}
                           onChange={(e) => {
                               dispatch(fillPassword(e.currentTarget.value))
                           }}/>
                    <label className="form__label" htmlFor="pass-input">Password</label>
                    <label className="form__show-pass"
                           onClick={() => togglePasswordField.call(passwordField)}>👁</label>

                    <input className="form__input form__password"
                           id="pass-rep-input"
                           type="password"
                           placeholder=" "
                           ref={passwordRepeatField}
                           onChange={(e) => {
                               dispatch(fillRepeatedPassword(e.currentTarget.value))
                           }}/>
                    <label className="form__label" htmlFor="pass-rep-input">Repeat password</label>
                    <label className="form__show-pass"
                           onClick={() => togglePasswordField.call(passwordRepeatField)}>👁</label>

                    {inputState.error &&
                        <p className="form__error">{inputState.error}</p>
                    }

                    <input className="form__button"
                           type="submit"
                           value="Register"/>

                </fieldset>
            </form>
        </div>
    )
}

export default Register;