import {RegisterActionType, RegisterInputType, RegisterTypes} from './registerTypes';

function reducer(state: RegisterInputType, action: RegisterActionType): RegisterInputType {
    switch (action.type) {
        case RegisterTypes.FILL_USERNAME:
            return {...state, usernameInput: action.payload, error: ''};
        case RegisterTypes.FILL_PASSWORD:
            return {...state, passwordInput: action.payload, error: ''};
        case RegisterTypes.FILL_REPEATED_PASSWORD:
            return {...state, passwordRepeatInput: action.payload, error: ''}
        case RegisterTypes.SET_IS_VALID:
            return {...state, error: action.payload}
        default:
            return state;
    }
}

export const fillUsername = (payload: string) => {
    return {type: RegisterTypes.FILL_USERNAME, payload}
};

export const fillPassword = (payload: string) => {
    return {type: RegisterTypes.FILL_PASSWORD, payload}
};

export const fillRepeatedPassword = (payload: string) => {
    return {type: RegisterTypes.FILL_REPEATED_PASSWORD, payload}
};

export const setIsValid = (payload: string) => {
    return {type: RegisterTypes.SET_IS_VALID, payload}
};

export default reducer;