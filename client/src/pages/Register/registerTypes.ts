export interface RegisterInputType {
    usernameInput: string;
    passwordInput: string;
    passwordRepeatInput: string;
    error: string;
}

export enum RegisterTypes {
    FILL_USERNAME = 'FILL_USERNAME',
    FILL_PASSWORD = 'FILL_PASSWORD',
    FILL_REPEATED_PASSWORD = 'FILL_REPEATED_PASSWORD',
    SET_IS_VALID = 'SET_IS_VALID'
}


interface FILL_USERNAME_ACTION {
    type: RegisterTypes.FILL_USERNAME,
    payload: string
}

interface FILL_PASSWORD_ACTION {
    type: RegisterTypes.FILL_PASSWORD,
    payload: string
}

interface FILL_REPEATED_PASSWORD_ACTION {
    type: RegisterTypes.FILL_REPEATED_PASSWORD,
    payload: string
}

interface SET_IS_VALID_ACTION {
    type: RegisterTypes.SET_IS_VALID;
    payload: string;
}

export type RegisterActionType =
    FILL_USERNAME_ACTION
    | FILL_PASSWORD_ACTION
    | FILL_REPEATED_PASSWORD_ACTION
    | SET_IS_VALID_ACTION