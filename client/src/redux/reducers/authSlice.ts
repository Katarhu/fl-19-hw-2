import {createSlice, createAsyncThunk, PayloadAction} from '@reduxjs/toolkit';
import {UserState, UserType, AuthResponseType} from "../../models/User";
import axios from '../../utils/axios';
import {RootState} from "../store";
import {useAppDispatch} from "../../hooks/redux";


const initialState: UserState = {
    user: null,
    token: null,
    isLoading: false,
    error: ''
}

interface authType {
    username: string;
    password: string
}

export const registerUser = createAsyncThunk(
    'auth/registerUser',
    async ({username, password}: authType, thunkAPI) => {
        try {
            const {data} = await axios.post('/auth/register', {
                username,
                password
            });

            window.localStorage.setItem('token', data.token);

            return data;
        } catch (err) {
            return thunkAPI.rejectWithValue('Failed to register user');
        }
    }
);

export const loginUser = createAsyncThunk(
    'auth/loginUser',
    async ({username, password}: authType, thunkAPI) => {
        try {
            const {data} = await axios.post('/auth/login', {
                username,
                password
            });

            window.localStorage.setItem('token', data.jwt_token);

            return data;
        } catch (err) {
            return thunkAPI.rejectWithValue('Failed to login')
        }
    }
)

export const authUser = createAsyncThunk(
    'auth/',
    async (_, thunkAPI) => {
        try {
            const {data} = await axios.get('/auth');

            return data;
        } catch {

        }
    }
)

export const deleteUser = createAsyncThunk(
    'user/delete',
    async (_, thunkAPI) => {
        try {
            await axios.delete('/users/me');
        } catch (err) {
            return thunkAPI.rejectWithValue('Failed to login');
        }
    }
)

export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        logout: (state) => {
            state.user = null;
            state.token = null;
            state.isLoading = false;
            state.error = '';
        }
    },
    extraReducers: {
        [registerUser.pending.type]: (state) => {
            state.isLoading = true;
            state.error = '';
        },
        [registerUser.fulfilled.type]: (state, action: PayloadAction<AuthResponseType>) => {
            state.user = action.payload.user
            state.token = action.payload.token;
            state.isLoading = false;
            state.error = '';
        },
        [registerUser.rejected.type]: (state, action: PayloadAction<string>) => {
            state.isLoading = false;
            state.error = action.payload;
        },
        [loginUser.pending.type]: (state) => {
            state.isLoading = true;
            state.error = '';
        },
        [loginUser.fulfilled.type]: (state, action: PayloadAction<AuthResponseType>) => {
            state.user = action.payload.user
            state.token = action.payload.jwt_token;
            state.isLoading = false;
            state.error = '';
        },
        [loginUser.rejected.type]: (state, action: PayloadAction<string>) => {
            state.isLoading = false;
            state.error = action.payload;
        },
        [authUser.pending.type]: (state) => {
            state.isLoading = true;
            state.error = '';
        },
        [authUser.fulfilled.type]: (state, action: PayloadAction<AuthResponseType>) => {
            state.user = action.payload?.user;
            state.isLoading = false;
            state.error = '';
        },
        [authUser.rejected.type]: (state, action: PayloadAction<string>) => {
            state.isLoading = false;
            state.error = action.payload;
        }
    }
});

export default authSlice.reducer;

export const {logout} = authSlice.actions;

export const checkIsAuth = (state: RootState) => Boolean(state.auth.user);