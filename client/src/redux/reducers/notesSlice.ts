import {createSlice, createAsyncThunk, PayloadAction} from '@reduxjs/toolkit';
import axios from '../../utils/axios';
import {NoteState, Note, NoteResponse, NotesResponse} from "../../models/Note";
import {AuthResponseType} from "../../models/User";
import {authUser, loginUser, registerUser} from "./authSlice";
import {act} from "react-dom/test-utils";

const initialState: NoteState = {
    notes: [],
    isLoading: false,
    error: ''
}

interface queryType {
    limit: number;
    offset: number;
}

export const getNotes = createAsyncThunk(
    'notes/getNotes',
    async ({limit, offset}: queryType, thunkAPI) => {
        try {
            const {data} = await axios.get(`/notes/?limit=${limit}&offset=${offset}`,);

            return data;
        } catch (err) {
            return thunkAPI.rejectWithValue('Failed to get notes');
        }
    }
)


export const deleteNoteById = createAsyncThunk(
    'notes/deleteNotes',
    async (id: string, thunkAPI) => {
        try {
            await axios.delete(`/notes/${id}`);
            return id;
        } catch (err) {
            return thunkAPI.rejectWithValue('Failed to delete note');
        }
    }
)

export const createNote = createAsyncThunk(
    'notes/addNotes',
    async (text: string, thunkAPI) => {
        try {
            const {data} = await axios.post('/notes', {
                text
            });

            return data;
        } catch (err) {
            return thunkAPI.rejectWithValue('Failed to create note');
        }
    }
)

export const noteSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        clearNotes: (state) => {
            state.notes = [];
        }
    },
    extraReducers: {
        [getNotes.pending.type]: (state) => {
            state.isLoading = true;
        },
        [getNotes.fulfilled.type]: (state, action: PayloadAction<NotesResponse>) => {
            state.isLoading = false
            state.notes = action.payload.notes;
            state.error = '';
        },
        [getNotes.rejected.type]: (state, action: PayloadAction<string>) => {
            console.log(action)
            state.isLoading = false;
            state.error = action.payload;
        },
        // DELETE NOTE
        [deleteNoteById.pending.type]: (state) => {
            state.isLoading = true;
        },
        [deleteNoteById.fulfilled.type]: (state, action: PayloadAction<string>) => {
            state.notes = state.notes.filter(note => note._id !== action.payload);
            state.isLoading = false;
            state.error = '';
        },
        [deleteNoteById.rejected.type]: (state, action: PayloadAction<string>) => {
            state.isLoading = false;
            state.error = action.payload;
        },
        //ADD NOTE
        [createNote.pending.type]: (state) => {
            state.isLoading = true;
        },
        [createNote.fulfilled.type]: (state, action: PayloadAction<NoteResponse>) => {
            state.notes.push(action.payload.note);
            state.isLoading = false;
            state.error = '';
        },
        [createNote.rejected.type]: (state, action: PayloadAction<string>) => {
            state.isLoading = false;
            state.error = action.payload;
        }
    }
});

export const {clearNotes} = noteSlice.actions;

export default noteSlice.reducer;
