const User = require('../models/User');
const ApiError = require('../error/ApiError');
const bcrypt = require('bcryptjs');
const generateJWT = require('../helpers/generateJWT');

class AuthController {
    async register(req, res, next) {
        try {
            const {username, password} = req.body;

            if (!username || !password) {
                return next(ApiError.badRequest('Username or password is not provided'))
            }

            const isUser = await User.findOne({username});

            if (isUser) {
                return next(ApiError.badRequest('Username is already taken'));
            }

            const hashedPassword = await bcrypt.hash(password, 10);
            const newUser = new User({
                username,
                password: hashedPassword
            });

            await newUser.save();

            const token = generateJWT(username, newUser.id);

            return res.status(200).json({
                message: "Success",
                user: {
                    username,
                    id: newUser.id
                }, token
            });

        } catch (err) {
            return next(ApiError.internalError('Server error'));
        }
    }

    async login(req, res, next) {
        try {
            const {username, password} = req.body;

            if (!username || !password) {
                return next(ApiError.badRequest('Username or password is not provided'))
            }

            const user = await User.findOne({username});

            if (!user) {
                return next(ApiError.badRequest('Username or password is incorrect'));
            }

            const isPasswordCorrect = await bcrypt.compare(password, user.password);

            if (!isPasswordCorrect) {
                return next(ApiError.badRequest('Username or password is incorrect'));
            }

            const token = generateJWT(user.username, user.id);

            return res.status(200).json({
                message: "Success", jwt_token: token,
                user: {
                    username: user.username,
                    id: user.id
                }
            });

        } catch (err) {
            return next(ApiError.internalError('Server error'));
        }
    }

    async checkAuth(req, res, next) {
        try {
            const user = req.user;

            const isUser = await User.findOne({id: user.id});

            if (!isUser) {
                next(ApiError.unauthorized('Not authorized'));
            }

            return res.status(200).json({user: {username: user.username, id: user.id}});

        } catch (err) {
            return next(ApiError.internalError('Server error'));
        }
    }
}

module.exports = new AuthController();