const Router = require('express');
const router = new Router();
const authController = require('../controllers/auth.controller');
const authMiddleware = require('../middleware/authMiddleware');

router.post('/register', authController.register);
router.post('/login', authController.login);
router.get('/', authMiddleware, authController.checkAuth);

module.exports = router;
